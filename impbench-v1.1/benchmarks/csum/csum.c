/*  + Name:
 *  ImpBench/checksum
 *
 *  + Details:
 *  This program is a modified version of the "cksum.c" source. It calculates the chechsum of any ascii
 *  or binary file. It reads the whole file into a buffer, calculates the total checksum, copies the
 *  whole file to a separate output file and appends the calculated checksum to the end of the same file.
 *  This copy of data is actually done to avoid writing the input file. Writing to the output can be
 *  easily disabled and the checksum results returned by the main function.
 * 
 *  + Syntax:
 *  <exec> <in filename> <out filename>
 *
 *  + Copyright:
 *  The initial source is the property of the original authors, as stated below.
 *  This implementation is the property of:
 *  Christos Strydis and Christofer Kachris, CE Lab, TU Delft, 2008. All rights reserved (C).
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>


// Compute checksum for "count" bytes beginning at location "addr".
long checksum (unsigned short * addr, int count) {
  register unsigned long sum = 0;

  while( count > 1 )
    {
      sum+= ~((unsigned short)*addr++);
      count -= 2;
    }

  /*  Add left-over byte, if any */
  if( count > 0 )
    sum += * (unsigned char *) addr;

  /*  Fold 32-bit sum to 16 bits */
  while (sum>>16)
    sum = (sum & 0xffff) + (sum >> 16);

#ifdef LITTLE_ENDIAN
  sum = ((sum >> 8) & 0xFF) + ((sum << 8) & 0xFF00);
#endif

  return sum;
}



int main(int argc, char** argv) {
	unsigned short * buf;
	int n, m;
	long cksum_result = 0;
	FILE * fd_in, * fd_out;


	fd_in = fopen(argv[1], "rb");
	if (feof(fd_in)) {
		return -1;
	}

	fd_out = fopen(argv[2], "wb");
	if (feof(fd_out)) {
		return -1;
	}

	fseek(fd_in, 0L, SEEK_END);
	n = ftell(fd_in);
	rewind(fd_in);

	m = (int)((n/2.0)+0.5); // calculate the memory size needed
	buf = (unsigned short *) malloc (m*sizeof(unsigned short));
	//memset(buf, 0x00, n);

	n = fread (buf, 1, m*sizeof(unsigned short), fd_in); // read all data from file
	printf("\nBytes read from input file: %d\n", n);


	cksum_result = checksum(buf, n); // calculate the checksum of the whole input file


	printf ("\nChecksum: %lu (%d bytes read from file).\n", cksum_result, n);


	n = fwrite (buf, 1, m*sizeof(unsigned short), fd_out); // written all data to file
	n += fwrite (buf, 1, sizeof(long), fd_out);	           // plus the checksum
	printf("\nBytes written to output file: %d\n", n);


	fclose(fd_in);
	fclose(fd_out);
	free(buf);

	return 0;
}
