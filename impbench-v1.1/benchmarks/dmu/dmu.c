/*  + Name:
 *  ImpBench/DMU
 *
 *  + Details:
 *  Drug-delivery & Monitoring (DMU) system simulator, based on the description given in
 *  the paper:
 *  P. S. Cross, R. Kunnemeyer, C. R. Bunt, D. A. Carnegie and Michael J. Rathbone,
 *  "Control, communication and monitoring of intravaginal drug delivery in dairy cows",
 *  International Journal of Pharmaceutics, Vol. 282, Issues 1-2, 10 September 2004,
 *  pp. 35--44.
 *  
 *  This is a kernel program. It does (and can) not simulate all real-time time aspects
 *  of the actual system, such as initialisation steps. Concisely, it does not currently
 *  simulate:
 *  + Most initialisation, configuration and calibration operations of the system
 *    peripheral components such as sensors and actuator.
 *  + Receiver module and assorted operations such as CowReceptionEvents.
 *  + "Doze mode" is meaningless without actual sleep states being meaningfully
 *    simulated in the kernel.
 *  + Peripherals-specific read/write patterns could not be meaningfully simulated or
 *    were unknown and were, thus, omitted. Some fake variables have been added, though,
 *    to emulate setting/clearing of enable pins or flags. They are denoted throughout
 *    the code as: (dummy).
 *  Most of these operations are not important for what the kernel attempts to capture;
 *  that is, no real-time behavior of the system but rather the instruction mix and
 *  frequency. Also, an attempt has been made to preserve defines, variable/function
 *  names, functionalities and types as in the original source, for consistency and
 *  maintenance reasons.
 *
 *  Pressure and Temp files must contain the same number N of data. The Integr-Current
 *  file must contain M data entries with the first of them being the supposed current
 *  readout at pressurization point and the remaining M-1 of them being the
 *  post-pressurization readouts. It must hold that M <= N.
 *  Pressure, temperature and integr-current files have been created based on the data
 *  found in "Water 8 - mass log.xls". (For access to this file, please contact the
 *  primary author directly. Send your requests to: Peter.Cross@agresearch.co.nz).
 *
 *  To-Do list:
 *  - Simulate the functionality of function: gas_mon_calibrate_current()
 *  - Simulate the functionality of function: TC1073_Vreg_on_decision()
 *  - Simulate the functionality of the TXR_receive_wakeUpCause timeout interrupt for
 *    more accurate worst-case behavior simulation (on (unexpected) data reception).
 *
 *  + Syntax:
 *  <exec> <pressure in filename> <temperature in filename>
 *         <actual integrated-current in filename> <transmissions out filename>
 *
 *  + Copyright:
 *  The delivery-monitoring unit (dmu) concept and sources are the property of the
 *  authors, as stated below. This implementation is the property of:
 *  Christos Strydis, CE Lab, TU Delft, 2008. All rights reserved (C).
 *
 *  Original version written by P. S. Cross, Department of Physics and Electronic
 *  Engineering, University of Waikato, 2002.
 */


#include<stdio.h>
#include<stdlib.h>

// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// Define's taken from the original source
typedef unsigned char byte;
typedef unsigned int word;
typedef unsigned char bool;


#define MANYCYCLES 3870 // original, full execution
#define FEWCYCLES  595  // reduced execution (for generating about 10KB of output data)


#ifndef TRUE 
    #define TRUE (byte)0xFF
#endif

#ifndef FALSE
    #define FALSE (byte)0x00
#endif

#ifndef ON
    #define ON (byte)0xFF
#endif

#ifndef OFF
    #define OFF (byte)0x00
#endif

#ifndef ACTIVE
    #define ACTIVE (byte)0xFF
#endif

#ifndef INACTIVE
    #define INACTIVE (byte)0x00
#endif


//#define NO_SLEEP_FOR_DEFAULT_S 2.5 // time in seconds that MCU stays at full speed for. max = 63.9f
//                                   // Note:  The MCU shuts down at the end of this period
//                                   // even if data is being received, so don't make it too short!
				                     // the bigger this is, the longer the idle waking period
                				     // (ENABLE IF TRANSCEIVER SECTION IS ENABLED)

// rounding macros
// unsigned rounding and casting
#define byte_round_cast(i)		((byte)((i)+0.5))
#define word_round_cast(i)		((word)((i)+0.5))
#define unsigned_int_round_cast(i)	((unsigned int)((i)+0.5))



#define GASCELL_CHECK_PERIOD 6 // time in minutes between carrying out gascell decision check
			                   // the gascell check also occurs immediately before a result set storage event so if RECORD_RESULTS_PERIOD < GASCELL_CHECK_PERIOD 
			                   // the effective gascell decision check period is RECORD_RESULTS_PERIOD
			                   // hence the following #define which can be used to check the actual period

#define RECORD_RESULTS_PERIOD 6 // time in minutes between capturing a result set for data logging

#define TRANSMIT_ID_PERIOD 30 // Time in minutes between ID transmissions. This should be at least one more than the ID number of the DMU with the largest ID number.  
                              // This is because although the period between each ID transmission is TRANSMIT_ID_PERIOD, the ID gets transmitted x minutes before the
			                  // end of the ID transmission period, where x is the ID of the DMU. This is so that ID transmissions do not overlap.
                              // This should ideally be a multiple of the GASCELL_CHECK_PERIOD and RECORD_RESULTS_PERIOD and EVERY_ONCE_IN_A_WHILE_PERIOD,
                              // otherwise the ID transmissions might occur while the the other DMUs are busy doing something else.

#define EVERY_ONCE_IN_A_WHILE_PERIOD 40 // time in minutes between medium term housekeeping checks

#define MINUTES_INCREMENT 1L // The amount of minutes that get incremented on each tick. Set to one normally, but can be increased for acceleration

#define TIME_BASE_INTERVAL 60  // time in seconds between interrupts for main time base 60 seconds is the standard rate, but you can lower
                               // this to simulate accelerated time maximum value is 65.5534

#define INTEGRATED_I_TO_VOLUME_FACTOR_mAhr_per_mL 3.05 // units: mAhr/mL

#define PRESSURE_READINGS_TO_AVERAGE 5

#define PRESSURE_START_LIMIT 1090.0 // units: mbar. Pressure at which is assumed the syringe is pressurised and the delivery profile is activated.
                                    // Before this time, the gascell is full on.

#define PRESSURE_LOW_LIMIT 1090.0 // units: mbar

#define GO_ANYWAY_PRESSURE_START_TIME 400L // If the piston is very easy to get going, the pressure trigger level will never be reached, so this ensures
                                           // it happens after a certain amount of time anyway. Normal pressurisation time is 2 hours
                                           // Units: minutes. The average time to pressurisation is about 270 minutes

#define MAX_SAFE_PRESSURE 1350.0 // units: mbar
#define MAX_SAFE_GAS_VOLUME 100.0 // units: estimated mL dispensed.
#define MIN_SAFE_TEMPERATURE 0.0 // units: deg. C.
#define MAX_SAFE_TEMPERATURE 44.0 // units: deg. C.

#define ICA_TOP_UP_LIMIT 20.0 // The ICA register in the gascell monitor needs to be read and topped up 
                              //   every now and again since it can't hold the full capacity of a gascell
                              //   This is the value of the ICA in mAhr at which a check will make the decision to
                              //   trigger this action.

#define DEFAULT_GAS_MON_R_SENSE 3.99  // gascell monitor current sense resistor value in Ohms
#define DEFAULT_VREG_R_SENSE 2.2  // voltage regulator current sense resistor value in Ohms
#define DEFAULT_PRESSURE_OFFSET 0.0  // pressure offset in mbar
#define DEFAULT_TEMP_PRESS_SENSE_OFFSET 0.0  // temperature offset in deg. C for pressure sensor
#define DEFAULT_TEMP_GAS_MON_OFFSET 0.0  // temperature offset in deg. C for gascell monitor

#define MAX_READINGS 720 // maximum number of readings to take 
                         //   If it took a reading every 15min for 7 days, that would be
                         //   672 readings
                         // N.B.  You will probably try to set this to the maximum size
                         // ----  that will still fit in RAM.  When doing so, leave around 350
                         //       bytes spare RAM, otherwise the system crashes when the monitor
                         //       is present because the monitor uses about 282 bytes at the top
                         //       of RAM.

#define MAX_PROFILE_DATA_POINTS 30 // maximum number of vertices you can have on the profile graph

#define INFINITY_MINUTES 999999L // This is the time in minutes of the point on the profile
                                 // graph that should always be in the future.  It is an approximation
                                 // of infinity minutes.    999999 = 1.9 years
                                 // Don't make this number too big though, because
                                 // it is used in a calculation that should not overflow the resulting data
                                 // type.


#define ADC_10bit_FSD 1024 // full scale register value is 2^10 = 1024 for 10 bits

#define MON_TEMP_READINGS_TO_AVERAGE 5

// Conversion factors for compressing information (when reporting results)
//   Values get zipped (compressed) by being multiplied by these factors for storage
//   They are unzipped by dividing by these same factors

// gascell
#define GASCELL_V_FACTOR 127.0
#define GASCELL_I_FACTOR 200.0
#define ACTUAL_INTEGRATED_mAHr_FACTOR 100.0

// light
#define LIGHT_INTENSITY_FACTOR 2.5

// temperature
#define TEMP_FROM_MONITOR_FACTOR 100.0
#define TEMP_FROM_PRESS_FACTOR 100.0

// pressure
#define PRESSURE_FACTOR 10.0

// power supply
#define BATTERY_V_FACTOR 36.0
#define BATTERY_I_FACTOR 0.5
#define VDD_FACTOR 80.0
#define VREF_AVDD_FACTOR 80.0

// motion
#define MOTION_CYCLES_FACTOR 2.5
#define MOTION_PERCENT_FACTOR 2.5


// dummy data words supposedly read from sensor registers
#define DUMMY_CAL_WORD1 1 // Fake calibration words suppposedly read from the ADC output
#define DUMMY_CAL_WORD2 2 // used for our simulation purposes.
#define DUMMY_CAL_WORD3 3 //
#define DUMMY_CAL_WORD4 4 //
#define DUMMY_D1 1	      // Fake readout words supposedly read from the ADC output
#define DUMMY_D2 2	      // used for our simulation purposes.
#define DUMMY_DELAY 2	  // Artificial delay introduced to get near-identical readouts between this kernel and the real system
			              // It is used for correctly calcuating the Targer Volume to deliver since its calculation depends on the profile time
			              // which, in turn, depends on the time of pressurisation. Since the kernel operates in a synchronous fashion, opposite to the
			              // real system where events occur as interrupt events, this delay makes up for the error in the registered pressurisation time.

#define DUMMY_VREG_ERROR 0xFF    // Fake variable to emulate "VReg_Error_" status bit
#define DUMMY_VREG_SHUTDOWN 0xFF // Fake variable to emulate "VReg_Shutdown_" status bit

#define DUMMY_TEMP_LOWBYTE 0x1B  // Fake supposed temperature low-byte read of the gas monitor
#define DUMMY_TEMP_HIGHBYTE 0x00 // Fake supposed temperature high-byte read of the gas monitor
#define DUMMY_VOLT_LOWBYTE 0x1A  // Fake supposed voltage low-byte read of the gas monitor
#define DUMMY_VOLT_HIGHBYTE 0x00 // Fake supposed voltage high-byte read of the gas monitor


// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// Global-variable declarations

// ***DMU-specific variables***
unsigned int ID; // (dummy variable) (assigned for DMU3)

unsigned long accumulatedMotionCycles;
unsigned long motion_cycles_counter; // (dummy variable)
float accumulatedMotionPercentOpen;
unsigned long motion_percentopen_counter; // (dummy variable)

// counters and counter limits for performing specific tasks
const unsigned int gas_check_counter_limit = unsigned_int_round_cast((float)GASCELL_CHECK_PERIOD/(float)MINUTES_INCREMENT);
const unsigned int transmit_ID_counter_limit = unsigned_int_round_cast((float)TRANSMIT_ID_PERIOD/(float)MINUTES_INCREMENT);
const unsigned int record_results_counter_limit = unsigned_int_round_cast((float)RECORD_RESULTS_PERIOD/(float)MINUTES_INCREMENT);
const unsigned int everyOnceInAWhile_counter_limit = unsigned_int_round_cast((float)EVERY_ONCE_IN_A_WHILE_PERIOD/(float)MINUTES_INCREMENT);

unsigned int gas_check_counter;
unsigned int transmit_ID_counter;
unsigned int record_results_counter;
unsigned int everyOnceInAWhile_counter;

bool pressurisation_complete;
bool enablePressPort; // (dummy variable)
bool gascell_on; // (dummy variable)

// gascell sense resistor (taken for DMU3)
float gascellSenseResistor; // units: Ohms

// voltage regulator sense resistor (taken for DMU3)
float VregSenseResistor; // units: Ohms

// pressure reading offset (taken for DMU3)
float pressureCalOffset; // units: mbar

// temperature reading from pressure sensor offset (taken for DMU3)
float tempFromPressSensorCalOffset; // units: deg. C

// temperature reading from gascell monitor offset (taken for DMU3)
float tempFromGasMonitorCalOffset; // units: deg. C

const int full_gas_mon_ICA = 0xFF; // integrated current accumulator
int current_ICA; // (dummy variable)
bool DUMMY_ICA_REFRESH = FALSE;    // (dummy variable, needed for correct kernel operation)
bool DUMMY_GASCELL_CHECK = FALSE;  // (dummy variable, needed for correct kernel operation)
bool DUMMY_RECORD_RESULTS = FALSE; // (dummy variable, needed for correct kernel operation)

bool IAD; // Current A/D control bit (dummy variable)

float pressure_reading;

long timeAtPressurisation;
float pressureAtPressurisation;
float actualIntegratedI_mAhr_AtPress; // integrated current at pressurisation
float actualIntegratedI_mAhr;
float partialActualIntegratedI_mAhr;

float lastAveragePressure;
float lastAverageTemp;

float resultMotionPercentOpen;
long resultMotionCycles;

long minutes_count; // minutes since datum point


bool pressureLeakCompensationActive;

bool gasCellOverride; // overrides the gascell switch with the value
		      // stored in gasCellOverrideValue
bool gasCellOverrideValue; // overrides the gascell switch only if gasCellOverride Active
			   // is active.  You can directly control the gascell switch
			   // with these functions although if the pressure or volume safety
			   // limits are exceeded, a periodic check ensures that
			   // gasCellOverride = ACTIVE and that gasCellOverrideValue = OFF


bool fullWakeUp; // (dummy variable)
		 // Cleared when wait mode is entered with the main clock off, set by interrupts that need to wake
		 // the MCU as soon as possible to full speed.

bool TXR_receive_wakeUpCause; // Set by the relevant ISR that caused the receive interrupt for the main loop to check
bool tick_wakeUpCause; // Set by the relevant ISR that caused the wake up interrupt for the main loop to check


// ***I/O-used and other support variables***
FILE *fd_press; // pressure (mbar)
FILE *fd_temp; // temperature (deg. Celsius)
FILE *fd_current; // actual integrated current (mAhr)
FILE *fd_tx; // storing various transmitted implant-unit data

unsigned long DUMMY_COUNTER; // used to artifically emulate machine cycles


// ***Results-reporting related variables and settings***

// DMU status
union DMU_statusUnion {
	struct {
		char    gasCellOnOff_stat;         // gascell switch port setting status
		char    gasCellOverride_stat;      // gascell override active switch status
		char    gasCellOverrideValue_stat; // gascell override value switch status - irrelevant if gascell override not active
		char    VReg_Error_stat;           // voltage regulator unreg status
		char    VReg_Shutdown_stat;        // voltage regulator shutdown status
		char    anyReset_stat;             // if any cause of reset
		char    powerFailReset_stat;       // if power fail control from power supervisor
		char    watchDogReset_stat;        // if watch dog timer timed out
    } asBits;

    byte asbyte; 
};


// This structure has to hold the required information in the least space possible
//  to allow the most readings to be stored.  That's why floats are compressed into
//  bytes and words.  The results module explains this in detail.
struct resultSet {
	// DMU status
	union DMU_statusUnion DMU_status;

	// gascell
	byte gascellVoltageAsbyte; // (V)
	word gascellCurrentAsword; // (mA)
	word actualIntegratedmAhrAsWord; // (mAHr)

	// light
	byte lightIntensityAsbyte; // (% of Full Sun)

	// temperature
	word tempFromMonitorAsword; // (deg. C)
	word tempFromPressureSensorAsword; // (deg. C)

	// pressure
	word pressureAsword; // (mbar)

	// power supply
	byte batteryVoltageAsbyte; // (V)
	byte batteryCurrentAsbyte; // (mA) stored separately because voltages needed
				   //      to work this out are not stored with full precision
	byte VddAsbyte; // (V)
	byte Vref_and_AVdd_Asbyte; // (V) Vref is also AVdd

	// motion
	byte motionCyclesAsbyte; // (cycles)
	byte motionPercentOpenAsbyte; // (%)

	// cow proximity reception
	//union cowReceptionsUnion cowReceptions; // (not implemented in this kernel as transmissions from other cows cannot be emulated)
};


struct resultSet resultsLog[MAX_READINGS]; // this takes up about 65% RAM in the real system
unsigned int resultsLogIndex;		   // next reading number that will be written (Reading 0 is taken at minutes_count = 0)
       	                                   // NB:  Use resultsLogIndex-1 to get the last recorded results

float Vref; // typical value selected for faking function: float gas_mon_read_V_at_Vdd_Pin();
const word DUMMY_READ_ADC_CHANNEL1_10BIT = 0x0F; // typical value selected for faking function: word startAndReadADCchannel_10bit(byte channel);
const word DUMMY_READ_ADC_CHANNEL2_10BIT = 0x10; // value selected for faking function: word startAndReadADCchannel_10bit(byte channel);
const word DUMMY_READ_ADC_CHANNEL3_10BIT = 0x1A; // value selected for faking function: word startAndReadADCchannel_10bit(byte channel);
const float DUMMY_READ_CURRENT = 17.09; // typical value selected for faking function: float gas_mon_readCurrentAbs();
bool LED_Green;
bool LED_Red;
bool gas_mon_changeToVdd = FALSE;
bool gas_mon_changeToVad = FALSE;


// ***Delivery-profile related variables and settings***

// Delivery profile curve data points
//   These points are vertexes on the rate v.s. time curve for the drug delivery 
//   profile.
typedef struct dataPoint {
        long minute; // horizontal axis:  time (units: minutes since power on reset) 
                     // Not unsigned because time variables elsewhere can be negative 
                     // (profile time is negative before the pressurisation time.
                     //  It is easier to be consistent by always representing time with 
                     //  the same data type.
        float rate;  // vertical axis:  volume delivered (units: mL/day) these are most natural units for the user
} dataPoint;


char * activeProfileLabel = "Not yet assigned";  // will point to the active profile label
dataPoint activeDeliveryProfile[MAX_PROFILE_DATA_POINTS];  // pointer to the profile that will actually be used

/*
// Flat DMU Profile
//_________________
char flatProfileLabel[] = "flat DMU profile";
dataPoint flatDeliveryProfile[] = 
{ 
    {0, 0},                    // *** DO NOT CHANGE THIS LINE ***  Always start at {0, 0}  
                               // zero minutes means zero minutes from an affirmative pressurisation check
////////////// Your data points go below here: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//-------------------------------------------------------------------------------------------------
    {1, 4.29}, {10080, 4.29},  // A flat rate for 7 days at 4.29mL/day = 30mL  First day does not start until pressurisation complete
    {10081, 0},                // stop delivery after 7 days Experiment is over so continue at zero                   
//-------------------------------------------------------------------------------------------------
////////////// Your data points stop above here ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    {INFINITY_MINUTES, 0}      // *** DO NOT CHANGE THIS LINE ***  Always make this the last data point.  
};                             // The second to last data point should always have a rate of zero
                               // i.e. {x, 0} where x is the time in minutes that you wish to stop 
                               // delivering the solution.

// Varying DMU Profile
//____________________
char varyingProfileLabel[] = "varying DMU profile";
dataPoint varyingDeliveryProfile[] = 
{ 
    {0, 0},                    // *** DO NOT CHANGE THIS LINE ***  Always start at {0, 0}  
                               // zero minutes means zero minutes from an affirmative pressurisation check
////////////// Your data points go below here: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//-------------------------------------------------------------------------------------------------
    {1, 10}, {1440, 10},  // first day: flat 10mL/day.  First day does not start until pressurisation complete
    {2880, 0},   // decreases in a downward ramp to zero by end of day 2
    {7200, 0},   // start of upward ramp at beginning of day 6
    {8640, 10},  // day 7 is a flat 10mL/day 
    {10080, 10}, // end of day 7
    {10081, 0},  // start of day 8 - experiment over.  Note:  you can't have a vertical slope so that's
                 // why there is one minute separating these two points.
                 // Experiment is over so continue at zero                   
//-------------------------------------------------------------------------------------------------
////////////// Your data points stop above here ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    {INFINITY_MINUTES, 0}     // *** DO NOT CHANGE THIS LINE ***  Always make this the last data point.  
};                            // The second to last data point should always have a rate of zero
                              // i.e. {x, 0} where x is the time in minutes that you wish to stop 
                              // delivering the solution.
*/

// Test Profile 5  //  This is the varying profile curve speed up by 3
//________________
char test5ProfileLabel[] = "test5 DMU profile";
dataPoint test5DeliveryProfile[]= // wastes RAM - to do: pointer implementation
{ 
    {0, 0},			// *** DO NOT CHANGE THIS LINE ***  Always start at {0, 0}  
				// zero minutes means zero minutes from an affirmative pressurisation check
////////////// Your data points go below here: vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//-------------------------------------------------------------------------------------------------
//  This is the varying profile curve speed up by 3 in time and increased by 3 in rate
    {1, 30}, {480, 30},	 // first day: flat 30mL/day.  First day does not start until pressurisation complete
    {960, 0},		 // decreases in a downward ramp to zero by end of day 2
    {2400, 0},		 // start of upward ramp at beginning of day 6
    {2880, 30},		 // day 7 is a flat 30mL/day 
    {3360, 30},		 // end of day 7
    {3361, 0},		 // start of day 8 - experiment over.  Note:  you can't have a vertical slope so that's
			 // why there is one minute separating these two points.
			 // Experiment is over so continue at zero                   
//-------------------------------------------------------------------------------------------------
////////////// Your data points stop above here ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    {INFINITY_MINUTES, 0}	// *** DO NOT CHANGE THIS LINE ***  Always make this the last data point.  
};				// The second to last data point should always have a rate of zero
				// i.e. {x, 0} where x is the time in minutes that you wish to stop 
				// delivering the solution.

int i;

// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// Forward function declarations

double read_data (FILE * data_fd);
void write_result(FILE * data_fd, struct resultSet * thisResultSet);

float getTargetVolume_mL(long profile_time);
float getEstimatedVolume_mL_gasNotDrug (float actualIntegratedI_mAhr);
float getEstimatedVolume_mL_drugNotGas(float actualIntegratedI_mAhr, long profile_time);

void makeGascellSwitchDecision();
void doPressAndTempConversion();
void gascellSwitchSafetyCheck();

void ICA_refresh_check();
float getActualIntegratedI_mAhr();

void recordNextResultSet();
void takeResultSet(struct resultSet * thisResultSet);
byte getDMU_statusAsbyte();
byte zipGascellVoltageAsbyte();
float gas_mon_readCurrentAbs();
float gas_mon_readTemp();
float gas_mon_readVoltage();
word zipGascellCurrentAsword();
word zipActualIntegratedI_mAhrAsWord();
byte zipLightIntensityAsbyte();
word zipTempFromMonitorAsword();
word zipTempFromPressureSensorAsword();
word zipPressureAsword();
byte zipBatteryVoltageAsbyte();
byte zipBatteryCurrentAsbyte();
byte zipVddAsbyte();
byte zipVref_and_AVdd_Asbyte();
byte zipMotionCyclesAsbyte();
byte zipMotionPercentOpenAsbyte();

void resetTiltMotionCyclesAndPercentOpen();
void UpdateTiltMotionCyclesAndPercentOpen();

// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

// Main function
int main(int argc, char* argv[]) {

	// <exec> <pressure in filename> <temperature in filename> <actual integrated-current in filename> <transmissions out filename>
	if (argc != 5) {
		printf("\nWrong number of arguments!\n");
		return -1;
	}

	// Open pressure file for reading
	fd_press = fopen(argv[1],"r");
	if (!fd_press) {
		printf("\nCould not open %s file!\n", argv[1]);
		return -1;
	}

	// Open temperature file for reading
	fd_temp = fopen(argv[2],"r");
	if (!fd_temp) {
		printf("\nCould not open %s file!\n", argv[2]);
		return -1;
	}

	// Open integrated-current file for reading
	fd_current = fopen(argv[3],"r");
	if (!fd_current) {
		printf("\nCould not open %s file!\n", argv[3]);
		return -1;
	}

	// Open file for writing
	fd_tx = fopen(argv[4],"w");
	if (!fd_tx) {
		printf("\nCould not open %s file!\n", argv[4]);
		return -1;
	}



	// Program information
	printf("Drug-delivery- and Monitoring-Unit (DMU) benchmark kernel.\n");

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// ***INITIALIZATION PHASE***

	// Initialise Peripherals
	//-----------------------
	printf("\n\nInitialisation:"); // $DEBUG$
	printf("\n----------------");  //
	// > set active drug delivery profile
	for(i=0; i < MAX_PROFILE_DATA_POINTS; i++) {
		activeDeliveryProfile[i] = test5DeliveryProfile[i];
		activeProfileLabel = test5ProfileLabel;
	}
	printf("\nActive delivery profile: \"%s\"", activeProfileLabel);

	// >> initialize tilt-motion cycles and tilt-motion percent open
	accumulatedMotionCycles = 0L;
	motion_cycles_counter = 0xFFFF; // (dummy)
	resultMotionCycles = 0L;
	accumulatedMotionPercentOpen = 0.0;
	motion_percentopen_counter = 0xFFFF; // (dummy)
	resultMotionPercentOpen = 0.0;
/*	printf("\n\taccumulatedMotionCycles = %ld \n\tresultMotionCycles = %ld \n\taccumulatedMotionPercentOpen = %f \n\tresultMotionPercentOpen = %f", \
		accumulatedMotionCycles,resultMotionCycles,accumulatedMotionPercentOpen,resultMotionPercentOpen); // $DEBUG$
*/
	// >> initialize gas-cell monitor
	IAD = OFF;
	current_ICA = full_gas_mon_ICA; // (dummy)
	timeAtPressurisation = 0L; // units: min
	pressureAtPressurisation = 0.0; // units: mbar
	actualIntegratedI_mAhr_AtPress = 0.0; // units: mAhr
	actualIntegratedI_mAhr = 0.0; // units: mAhr
	partialActualIntegratedI_mAhr = 0.0; // units: mAhr
	IAD = ON;
	LED_Green = ON;
	LED_Red = ON;
/*	printf("\n\tIAD = %s \n\tcurrent_ICA = %d \n\ttimeAtPressurisation = %ld \n\tpressureAtPressurisation = %f \n\tactualIntegratedI_mAhr_AtPress = %f \n\tactualIntegratedI_mAhr = %f \n\tpartialActualIntegratedI_mAhr = %f", \
	(IAD==OFF)?"OFF":"ON",current_ICA,timeAtPressurisation,pressureAtPressurisation,actualIntegratedI_mAhr_AtPress,actualIntegratedI_mAhr,partialActualIntegratedI_mAhr); // $DEBUG$
*/
	// >> calibrate gas-cell current
	// >> initialize pressure sensor
	lastAveragePressure = 0.0; // units: mbar
	lastAverageTemp = 0.0; // units: deg. C
	pressurisation_complete = FALSE;
	pressureLeakCompensationActive = FALSE;
	gasCellOverride = INACTIVE;
	gasCellOverrideValue = OFF;
/*	printf("\n\tlastAveragePressure = %f \n\tlastAverageTemp = %f \n\tpressurisation_complete = %s \n\tpressureLeakCompensationActive = %s \n\tgasCellOverride = %s \n\tgasCellOverrideValue = %s", \
		lastAveragePressure,lastAverageTemp,(pressurisation_complete==FALSE)?"FALSE":"TRUE",(pressureLeakCompensationActive==FALSE)?"FALSE":"TRUE",(gasCellOverride==INACTIVE)?"INACTIVE":"ACTIVE",(gasCellOverrideValue==OFF)?"OFF":"ON"); // $DEBUG$
*/
	// >> initialize RTC time base
	minutes_count = 0;
    	gas_check_counter = 0; // (dummy)
    	transmit_ID_counter = 0;
	record_results_counter = 0;
    	everyOnceInAWhile_counter = 0;
/*	printf("\n\tminutes_count = %ld \n\tgas_check_counter = %u \n\ttransmit_ID_counter = %u \n\trecord_results_counter = %u \n\teveryOnceInAWhile_counter = %u", \
		minutes_count,gas_check_counter,transmit_ID_counter,record_results_counter,everyOnceInAWhile_counter); // $DEBUG$
*/

	// >> set device ID
	ID = 4; // assigned for DMU4
	//printf("\n\tID = %d", ID); // $DEBUG$

	// >> set calibration values
	gascellSenseResistor = 3.90; // units: Ohms
	VregSenseResistor = 2.20; // units: Ohms
	pressureCalOffset = 8.0; // units: mbar
	tempFromPressSensorCalOffset = 0.0; // units: deg. C
	tempFromGasMonitorCalOffset = 1.5; // units: deg. C
	Vref = 0.8; // units: V
/*	printf("\n\tgascellSenseResistor = %f \n\tVregSenseResistor = %f \n\tpressureCalOffset = %f \n\ttempFromPressSensorCalOffset = %f \n\ttempFromGasMonitorCalOffset = %f", \
		gascellSenseResistor,VregSenseResistor,pressureCalOffset,tempFromPressSensorCalOffset,tempFromGasMonitorCalOffset); // $DEBUG$
*/
	// >> initialize the core in sleep mode
	fullWakeUp = FALSE; // (dummy)

	// reset potential causes for waking up the core
	tick_wakeUpCause = FALSE;
	TXR_receive_wakeUpCause = FALSE; // RECEPTION OF DATA CANNOT BE PROPERLY MODELED IN THIS BENCHMARK.
					 // IT IS, THUS, ALWAYS DISABLED.
					 // IN THIS CASE, ALWAYS HOLDS THAT fullWakeUP == tick_wakeUpCause.

	// >> do first reading (index = 0) at time = 0 sec
	resultsLogIndex = 0;
	recordNextResultSet();


	// ***ENTER NORMAL-OPERATION PHASE***
	printf("\n\nNormal operation:"); // $DEBUG$
	printf("\n------------------"); //
	DUMMY_COUNTER = 0L;

	// write in output file the first line of labels for the recorded data to follow
	fprintf(fd_tx,"\t\t\t\tDMU\tVgas\tIgas\tIntIgas\tLight%%\tTgas\tTpsens\tP\tVbatt\tIbatt\tVdd\tVref\tMotion#\tMotion%%");

	// wait in sleep mode
	while (DUMMY_COUNTER < (MANYCYCLES*TIME_BASE_INTERVAL)) {
	        //| Only the tick wakeup and the noise rejecting RX INT0_interrupt on the TXR RX
	    	//| line can wake it up from sleep mode. They do that by setting fullWakeUp
	    	//| Polling mode is active: the RX polling interrupt itself
	    	//| will not cause this loop to finish, but if it finds
	    	//| real data during the rx polling, then fullWakeUp will
	    	//| be set and the loop inside snoozeToMainClock() will end.

		DUMMY_COUNTER ++;

		// every TIME_BASE_INTERVAL (default: 60) seconds, perform various tasks
		if ( (DUMMY_COUNTER % TIME_BASE_INTERVAL) == 0 ) {
			fullWakeUp = TRUE; // (dummy)
			tick_wakeUpCause = TRUE;

			// artificially update timers/counters - it's fine for simulation purposes
			//  we don't perform more elaborate schemes so as to minimize the impact on the kernel behavior
			//  since, in the real system, all these counters are hardware-updated, not by the CPU.
			if (IAD == ON) current_ICA -= 8; // simple pattern for current_ICA assumed here
							  // current_ICA counts down regardless of if the gascell is on or off.
			motion_cycles_counter -= 8;	  // simple pattern for motion_percentopen_counter assumed here
			motion_percentopen_counter -= 8;  // simple pattern for motion_cycles_counter assumed here
		}
		else
			continue; // remain in sleep mode

	        fullWakeUp = FALSE; // (dummy)  

        	// >> exit low-power configuration
	        // >> disable receiver polling                              

		// if receiver caused an interrupt (high-priority)
		if (TXR_receive_wakeUpCause) {
			/*
			TXR_receive_wakeUpCause = FALSE; 
			// >> delay for NO_SLEEP_FOR_DEFAULT_S seconds

			// >> disable receiver polling

			// loop here until all data received
			do {
				TXR_receive_wakeUpCause = FALSE;                
				// >> receive more data
			} while(TXR_receive_wakeUpCause);
			*/
		}

		// if clock-tick caused an interrupt (low-priority, every TIME_BASE_INTERVAL seconds)
		if (tick_wakeUpCause) {
			tick_wakeUpCause = FALSE;  

 			minutes_count += MINUTES_INCREMENT;
			//printf("\n\n-- %2.2ld:%2.2ld --", (minutes_count/60),(minutes_count%60)); // $DEBUG$

			// *** Update tilt-motion cycles and tilt-motion percent open counts (must be done at the end of each minute)
			//printf("\nTILT-MOTION COUNTERS TRIGGERED (1 min passed)"); // $DEBUG$
			UpdateTiltMotionCyclesAndPercentOpen();


			// *** If sufficient time has passed for checking the condition of the gascell
			if (++gas_check_counter >= gas_check_counter_limit) {
				//printf("\nGAS-CHECK COUNTER TRIGGERED (%u min passed)",gas_check_counter); // $DEBUG$
			        gas_check_counter = 0;

				// make a gascell check
				// (if pressurization is not complete, keep pressurizing; otherwise, turn gascell on/off tracking active delivery profile)
				makeGascellSwitchDecision();
			}

			// *** If sufficient time has passed for logging recorded results
			if (++record_results_counter >= record_results_counter_limit) {
				//printf("\nRECORD-RESULTS COUNTER TRIGGERED (%u min passed)",record_results_counter); // $DEBUG$
				record_results_counter = 0; // this is only for the timing of when to take results, 
                        			            // not an index into the resultsLog

				// record a new result set
				recordNextResultSet();
				write_result(fd_tx, &(resultsLog[resultsLogIndex-1])); // write results to file to emulate data storage (or transmission)
			}


			// *** If sufficient time has passed for performing long-term, periodic housekeeping tasks
			if (++everyOnceInAWhile_counter  >= everyOnceInAWhile_counter_limit) {
				//printf("\nHOUSEKEEPING COUNTER TRIGGERED (%u min passed)",everyOnceInAWhile_counter); // $DEBUG$
			        everyOnceInAWhile_counter = 0;

			        ICA_refresh_check();
			        //gas_mon_calibrate_current(); // (cannot be meaningfully simulated: involves some calibrating sensors with clear/set bits and wait states)
			        gascellSwitchSafetyCheck();
			        //TC1073_Vreg_on_decision(); // (cannot be meaningfully simulated: involves some calibrating sensors with clear/set bits and wait states)
			}


			// *** If sufficient time has passed for transmitting device ID
 			// All ID transmissions should occur with the same interval between them,
			// but we don't want them all to transmit at once.
			transmit_ID_counter ++; 
			if (transmit_ID_counter == (transmit_ID_counter_limit - ID) ) {
				//printf("\nTRANSMIT-ID COUNTER TRIGGERED (%u min passed)",transmit_ID_counter); // $DEBUG$
				fprintf(fd_tx,"\nTransmission @ %ld minutes >> ID: %d", minutes_count, ID); // write to file
			}
			if (transmit_ID_counter >= transmit_ID_counter_limit)
				transmit_ID_counter = 0;
        	    } // if (tick_wakeUpCause)
	} // while (!feof(fd1))


	// Close all opened files
	fclose(fd_press);
	fclose(fd_temp);
	fclose(fd_current);

	fclose(fd_tx);

	return 0;
}

// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// Function implementations

/////////////////////////////////--KERNEL-AUXILIARY FUNCTIONS--//////////////////////////////////////////////////
double read_data (FILE * data_fd) {
	char buffer[21];

	// check if more data are available in the file
	if (feof(data_fd)) {
		printf("\n>Found EOF. Could not read data.\n");
		exit(-1);
		//clearerr(data_fd);			  // $DEBUG$ (use input again and again for simulation purposes)
		//rewind(data_fd);			  //
		//printf("\n>Found EOF. Rewinding...\n"); //
	}

	// read next data block from file
	fgets(&buffer[0], 21*sizeof(char), data_fd);
	//printf(" [%f] ", atof(buffer)); // $DEBUG$ (print readout)
	return atof(buffer);
}


void write_result(FILE * data_fd, struct resultSet * thisResultSet) {

	if (feof(data_fd)) {
		printf("\n>EOF returned. Cannot write in output file.\n");
		exit(-1);
	}

	fprintf(data_fd,"\nReading: %.3d @ %.4ld min:", resultsLogIndex-1, minutes_count);
	fprintf(data_fd,"\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X\t0x%X", \
			thisResultSet->DMU_status.asbyte, thisResultSet->gascellVoltageAsbyte, thisResultSet->gascellCurrentAsword, \
			thisResultSet->actualIntegratedmAhrAsWord, thisResultSet->lightIntensityAsbyte, thisResultSet->tempFromMonitorAsword, \
			thisResultSet->tempFromPressureSensorAsword, thisResultSet->pressureAsword, thisResultSet->batteryVoltageAsbyte, \
			thisResultSet->batteryCurrentAsbyte, thisResultSet->VddAsbyte, thisResultSet->Vref_and_AVdd_Asbyte, thisResultSet->motionCyclesAsbyte, \
			thisResultSet->motionPercentOpenAsbyte);

	/*
	// Verbose printing of result data
	fprintf(data_fd,"------------(Reading: %u @ %ld minutes)------------\n", resultsLogIndex-1, minutes_count);
	fprintf(data_fd,"DMU status: 0x%X\n",thisResultSet->DMU_status.asbyte);
	fprintf(data_fd,"Gascell voltage: 0x%X\n",thisResultSet->gascellVoltageAsbyte);
	fprintf(data_fd,"Gascell current: 0x%X\n",thisResultSet->gascellCurrentAsword);
	fprintf(data_fd,"Gascell actual integrated current: 0x%X\n\n",thisResultSet->actualIntegratedmAhrAsWord);

	// light
	fprintf(data_fd,"Light intensity: 0x%X\n\n",thisResultSet->lightIntensityAsbyte);

	// temperature
	fprintf(data_fd,"Temp from gas monitor: 0x%X\n",thisResultSet->tempFromMonitorAsword);
	fprintf(data_fd,"Temp from pressure sensor: 0x%X\n\n",thisResultSet->tempFromPressureSensorAsword);

	// pressure
	fprintf(data_fd,"Pressure: 0x%X\n\n",thisResultSet->pressureAsword);
    
	// power supply
	fprintf(data_fd,"Battery voltage: 0x%X\n",thisResultSet->batteryVoltageAsbyte);
	fprintf(data_fd,"Battery current: 0x%X\n",thisResultSet->batteryCurrentAsbyte);
	fprintf(data_fd,"Vdd: %u\n",thisResultSet->VddAsbyte);
	fprintf(data_fd,"Vref and AVdd: 0x%X\n\n",thisResultSet->Vref_and_AVdd_Asbyte);

	// motion
	fprintf(data_fd,"Motion cycles: 0x%X\n",thisResultSet->motionCyclesAsbyte);
	fprintf(data_fd,"Motion percent-open: 0x%X\n",thisResultSet->motionPercentOpenAsbyte);
	fprintf(data_fd,"---------------------------------------------\n\n");
	*/
}

/////////////////////////////////--RESULT-RECORDING FUNCTIONS--//////////////////////////////////////////////////
void recordNextResultSet() {

	if (resultsLogIndex < MAX_READINGS) {
		resetTiltMotionCyclesAndPercentOpen();

		// update gascell switch position so it's setting is up to date for the result set
		// the decision is also made elsewhere on a more frequent basis but it is
		// checked here so that the gascell switch position is in a determinate state
		// at the time of taking the result set.
		DUMMY_RECORD_RESULTS = TRUE;  //* (dummy) for kernel correct operation, reading pressure/temperature from file is disabled since not enough entries exist
					      //* (similar to the DUMMY_ICA_REFRESH flag)
		makeGascellSwitchDecision();

		takeResultSet( &(resultsLog[resultsLogIndex++]) );
		DUMMY_RECORD_RESULTS = FALSE; //*

		//recentCowReceptions.asbyte = (byte)0; // reset for next recording period
							// (not implemented in this kernel as transmissions from other cows cannot be emulated)
	}
}


// Takes measurements and performs all actions on the given resultSet to update it with results freshly obtained during the call to this function.
// The argument is a pointer to the result set structure in question.
// NB:  This function is used for taking snapshots as well as data logging so do not perform any actions specific to either type of result gathering.
//      E.g. do not zero any tilt switch counters here for example.
void takeResultSet(struct resultSet * thisResultSet) {

	gas_mon_changeToVdd = TRUE;  // (dummy)
	gas_mon_changeToVad = FALSE; // (dummy)
	Vref = gas_mon_readVoltage(); // update Vref
	doPressAndTempConversion();   // 

	thisResultSet->DMU_status.asbyte = getDMU_statusAsbyte();
	thisResultSet->gascellVoltageAsbyte = zipGascellVoltageAsbyte();
	thisResultSet->gascellCurrentAsword = zipGascellCurrentAsword();
	thisResultSet->actualIntegratedmAhrAsWord = zipActualIntegratedI_mAhrAsWord();

	// light
	thisResultSet->lightIntensityAsbyte = zipLightIntensityAsbyte();

	// temperature
	thisResultSet->tempFromMonitorAsword = zipTempFromMonitorAsword();
	thisResultSet->tempFromPressureSensorAsword = zipTempFromPressureSensorAsword();

	// pressure
	thisResultSet->pressureAsword = zipPressureAsword();
    
	// power supply
	thisResultSet->batteryVoltageAsbyte = zipBatteryVoltageAsbyte();
	thisResultSet->batteryCurrentAsbyte = zipBatteryCurrentAsbyte();
	thisResultSet->VddAsbyte = zipVddAsbyte();
	thisResultSet->Vref_and_AVdd_Asbyte = zipVref_and_AVdd_Asbyte();

	// motion
	thisResultSet->motionCyclesAsbyte = zipMotionCyclesAsbyte();
	thisResultSet->motionPercentOpenAsbyte = zipMotionPercentOpenAsbyte();

	// cow proximity reception
	//thisResultSet->cowReceptions.asbyte = getCowReceptionsAsbyte(); // (not implemented in this kernel as transmissions from other cows cannot be emulated)
}


byte getDMU_statusAsbyte() { // no compression
	union DMU_statusUnion current_DMU_status;

	current_DMU_status.asBits.gasCellOnOff_stat = gascell_on; // gascell switch port setting status
	current_DMU_status.asBits.gasCellOverride_stat = gasCellOverride; // gascell override active switch status
	current_DMU_status.asBits.gasCellOverrideValue_stat = gasCellOverrideValue; // gascell override value switch status
	current_DMU_status.asBits.VReg_Error_stat = (byte)!DUMMY_VREG_ERROR; // voltage regulator unreg status
	current_DMU_status.asBits.VReg_Shutdown_stat = (byte)!DUMMY_VREG_SHUTDOWN; // voltage regulator shutdown status
	return current_DMU_status.asbyte;    
}


// Adds on voltage over gascell Rsense to get voltage over gascell only. Update Vref to an accurate value before calling this function.
byte zipGascellVoltageAsbyte() { // (V)
	float gasCell_V_WRT_GND; 
	float gasMonRsenseVoltage;

	gasCell_V_WRT_GND = (float)DUMMY_READ_ADC_CHANNEL1_10BIT/ADC_10bit_FSD*Vref;
	gasMonRsenseVoltage = gas_mon_readCurrentAbs()*gascellSenseResistor/1000.0; // division by 1000  to convert mA to A

	// adds on the voltage over gascell Rsense to get voltage over gascell only
	return( byte_round_cast( (gasCell_V_WRT_GND + gasMonRsenseVoltage) * GASCELL_V_FACTOR )  );
}


// Read the current and take the absolute value of it. Will return +ve value both for charging and discharging
// Units: mA
float gas_mon_readCurrentAbs() {
	float signed_current = DUMMY_READ_CURRENT;
	return ( (signed_current > 0)? signed_current:-signed_current );
}


word zipGascellCurrentAsword() { // (mA) 
	return ( word_round_cast(gas_mon_readCurrentAbs() * GASCELL_I_FACTOR) );
}


word zipActualIntegratedI_mAhrAsWord() {
	return ( word_round_cast(getActualIntegratedI_mAhr()*ACTUAL_INTEGRATED_mAHr_FACTOR) );
}



// % of Full Sun:  0% = full dark, 100% = very bright light
// Not calibrated. Not linear. Must turn LEDs off during reading.
byte zipLightIntensityAsbyte() { // (% of Full Sun)
	byte light_reading;
	bool restore_LED_Green;
	bool restore_LED_Red;

	restore_LED_Green = LED_Green;
	LED_Green = OFF;

	restore_LED_Red = LED_Red;
	LED_Red = OFF;

	light_reading = byte_round_cast( ((float)DUMMY_READ_ADC_CHANNEL2_10BIT) / ADC_10bit_FSD * 100.0 * LIGHT_INTENSITY_FACTOR ); // multiply by 100 to get %

	LED_Green = restore_LED_Green;
	LED_Red = restore_LED_Red;

	return light_reading;
}


word zipTempFromMonitorAsword() { // (deg. C)
	return ( word_round_cast(gas_mon_readTemp() * TEMP_FROM_MONITOR_FACTOR) );
}

float gas_mon_readTemp() {
	word temperature_lowbyte, temperature_highbyte;
	short temp;
	int this_reading;
	double accumulatedTemperature = 0;

	for(this_reading = 0; this_reading < MON_TEMP_READINGS_TO_AVERAGE ; this_reading++) {
		//>> start temp conversion

		// read scratchpad
		temperature_lowbyte = DUMMY_TEMP_LOWBYTE; // read LSB of temperature
		temperature_highbyte= DUMMY_TEMP_HIGHBYTE; // read MSB of temperature

		//>> terminate read sequence since we don't want anything else

		// convert from 13 bit signed int where each count represents 0.03125 degrees Celsius.  The last 3 bits are always 000.  
		// 11001001 00000000 = 0xC900 = -55.0000 degrees Celsius MSB is whole parts of degrees, MS bit is a sign bit
		// LSB is whole fractional parts of degrees. See page 4 of the DS2438 data sheet
		temp = temperature_highbyte<<8;
		temp = temp|temperature_lowbyte;
		accumulatedTemperature += ((double)temp)/256.0; // convert
	}
	return ( (float)(accumulatedTemperature / MON_TEMP_READINGS_TO_AVERAGE ) + tempFromGasMonitorCalOffset );
}

word zipTempFromPressureSensorAsword() { // (deg. C)
	return ( word_round_cast((lastAverageTemp + tempFromPressSensorCalOffset) * TEMP_FROM_PRESS_FACTOR) );
}


word zipPressureAsword() { // (mbar)
	return word_round_cast((lastAveragePressure + pressureCalOffset) * PRESSURE_FACTOR);
}


byte zipBatteryVoltageAsbyte() {
	//  Vad pin on gascell monitor is +Battery
	gas_mon_changeToVdd = FALSE;  // (dummy)
	gas_mon_changeToVad = TRUE; // (dummy)
	return byte_round_cast(gas_mon_readVoltage() * BATTERY_V_FACTOR);
}


// Instantaneous gascell current.
// The sense resistor is between AVdd and Vdd. Vref is connected to AVdd and the ADC input is connect to Vdd, so the sense
// resistor is really connected between Vref and the ADC input. The more the voltage drop across the sense resistor, the lower the ADC reading.
// If there was no current flow at all, then there would be no voltage drop and the ADC would be at the same voltage as Vref, i.e. it would read full scale then.
// So to get a current reading, the register value must be subtracted from the full scale reading.
byte zipBatteryCurrentAsbyte() { // (mA)
	return ( byte_round_cast( (ADC_10bit_FSD - (float)DUMMY_READ_ADC_CHANNEL3_10BIT) * 1000.0 / ADC_10bit_FSD * Vref / VregSenseResistor * BATTERY_I_FACTOR ) );
}


byte zipVddAsbyte() {
    return ( byte_round_cast( ((float)DUMMY_READ_ADC_CHANNEL3_10BIT) / ADC_10bit_FSD * Vref * VDD_FACTOR ) );

}


byte zipVref_and_AVdd_Asbyte() { //  Vref is also AVdd
	//  Vdd pin on gascell monitor is AVdd = Vref
	gas_mon_changeToVdd = TRUE;  // (dummy)
	gas_mon_changeToVad = FALSE; // (dummy)
	Vref = gas_mon_readVoltage(); // update Vref

	return ( byte_round_cast(Vref * VREF_AVDD_FACTOR) );
}


float gas_mon_readVoltage() {
	word voltage_lowbyte, voltage_highbyte;

	//>> start voltage conversion

	// read scratchpad
	voltage_lowbyte = DUMMY_VOLT_LOWBYTE; // read LSB of voltage
	voltage_highbyte= DUMMY_VOLT_HIGHBYTE; // read MSB of voltage

	//>> terminate read sequence since we don't want anything else

	// convert data
	// See page 4 of the DS2438 data sheet
	return ( ((float)(voltage_highbyte|voltage_lowbyte))/100 );
}


byte zipMotionCyclesAsbyte() { // (cycles)
	// do not zero getTiltMotionCycles here because it may be a snapshot
	return ( byte_round_cast( resultMotionCycles * MOTION_CYCLES_FACTOR) );
}


byte zipMotionPercentOpenAsbyte() { // (%)
	// do not zero getTiltMotionPercentOpen here because it may be a snapshot
	return byte_round_cast( resultMotionPercentOpen * MOTION_PERCENT_FACTOR);    
}

/////////////////////////////////--NORMAL-OPERATION FUNCTIONS--//////////////////////////////////////////////////
// If override is not active, compares estimated volume of drug delivered to delivery profile.
// If behind, switches the gascell on.  If ahead, switches the gascell off.
void makeGascellSwitchDecision() {
	float pressure_reading;
	float EstimatedVolume_mL_drugNotGas = 0.0;
	float TargetVolume_mL = 0.0;

	doPressAndTempConversion(); // for getting pressure
	pressure_reading = lastAveragePressure + pressureCalOffset; // get pressure

	pressureLeakCompensationActive = FALSE;

	// if automatic gascell control has been overriden, force the gascell on
	if (gasCellOverride == ACTIVE) {
		if (gasCellOverrideValue == ON) {
			//printf("\nGascell override active. Switching gascell on..."); // $DEBUG$
			gascell_on = TRUE; // (dummy)
		}
		else {
			//printf("\nGascell override inactive. Switching gascell off..."); // $DEBUG$
			gascell_on = FALSE; // (dummy)
		}
	}
	else { // gasCellOverride is INACTIVE
		// if pressurization has not been achieved yet since initialization, run this branch until it does
		if  (pressurisation_complete == FALSE) {
			//printf("\nPressurisation not complete. Pressurising..."); // $DEBUG$
			gascell_on = TRUE;  // max out the gascell until pressurisation is complete (dummy)

			// if pressure has reached right value or a time threshold has been crossed, start delivery; otherwise continue pressurizing
			if ( (pressure_reading > PRESSURE_START_LIMIT) || (minutes_count > GO_ANYWAY_PRESSURE_START_TIME) ) {
				//printf("\nPressurisation complete (pressure point or time-out has been reached). Delivering in next iteration..."); // $DEBUG$
				// start delivery
				pressurisation_complete = TRUE;
				timeAtPressurisation = minutes_count + DUMMY_DELAY; // return number of active cycles (experimental data: 92 min)
										    // (DUMMY_DELAY is artificial delay added to get identical to the real readouts)
				pressureAtPressurisation = pressure_reading;

				// get actual integrated current at pressurization (in mAhr)
				actualIntegratedI_mAhr_AtPress = getActualIntegratedI_mAhr(); // (experimental data: 20.16 mAhr)

				// zero actual integrated current
				partialActualIntegratedI_mAhr = 0.0;

				// now that the gascell is pressurised, the target volume must be compared against the estimated volume (in the following cycles)
				// from this point onwards trigger the target profile tracking mechanism
				/* printf("\nAt pressurisation:\nPressure: %f mbar.\nTime: %ld min.\nIntegrated current: %f mAhr.", \
						pressure_reading, minutes_count, actualIntegratedI_mAhr_AtPress); // $DEBUG$ */
			}
		}
		else { // pressurisation_complete == TRUE
		// When the syringe leaks and there is no programmed drug delivery for some time, the pressure decreases. This means a delay when the delivery profile starts
		//   to deliver drug again.  To combat this, the gascell is turned on to keep the pressure at a certain minimum level, even when no drug is delivered.
		//   Since this does not represent any programmed delivery, the integrated current accumulator (ICA) is disabled when this happens.

			// if pressure has reached right value or a time threshold has been crossed, start delivery; otherwise continue pressurizing
			if (pressure_reading > PRESSURE_LOW_LIMIT) {
				//printf("\nPressurisation already complete and pressure above lower limit."); // $DEBUG$
				pressureLeakCompensationActive = FALSE;
				gascell_on = FALSE; // (dummy) Ensure turn off first
				IAD = ON; // (dummy) Make sure integrated current accumulator (ICA) is enabled.
				//  Do this exactly like this.  I think the technique of always setting the the IAD bit, then clearing it if the pressure was less than the
				//  limit caused me some grief with ICA incrmentrenting when it was not supposed to.

				EstimatedVolume_mL_drugNotGas = getEstimatedVolume_mL_drugNotGas(getActualIntegratedI_mAhr(), minutes_count - timeAtPressurisation);
				TargetVolume_mL = getTargetVolume_mL(minutes_count - timeAtPressurisation);

				if (EstimatedVolume_mL_drugNotGas < TargetVolume_mL) {
					/*printf("\nSwitching gascell on: [EstimatedVolume_mL_drugNotGas < TargetVolume_mL] = [%f mL < %f mL]", \
						EstimatedVolume_mL_drugNotGas,TargetVolume_mL); // $DEBUG$*/
					gascell_on = TRUE; // (dummy)
				}
				else {
					/*printf("\nSwitching gascell off: [EstimatedVolume_mL_drugNotGas >= TargetVolume_mL] = [%f mL >= %f mL]", \
						EstimatedVolume_mL_drugNotGas,TargetVolume_mL); // $DEBUG$*/
					gascell_on = FALSE; // (dummy)
				}
			}
			else { // below delivery pressure so turn gascell on without enabling mAhr counting
				pressureLeakCompensationActive = TRUE;
				IAD = OFF; // (dummy)
				gascell_on = TRUE; // (dummy)
			}
		}
	}
}


void doPressAndTempConversion() {
	long dut, c1, c2, c3, c4, c5, c6, d1, d2;			             // variables used for calculating the pressure
	long calWord1, calWord2, calWord3, calWord4, ut20, off, sens, p; // and temperature read out from the gas-cell
	long dut2, t;							                         // monitor. (names and calculations replicated here from the original)
	int this_reading;						                         //
	long p_accum = 0;						                         //
	long t_accum = 0;						                         //

    
	enablePressPort = TRUE; // (dummy)

	// (inlining of original press() and temp() functions)
	for (this_reading = 0; this_reading < PRESSURE_READINGS_TO_AVERAGE; this_reading++) {

		// calculate coefficients
	       	calWord1 = DUMMY_CAL_WORD1; // (dummy)
	       	calWord2 = DUMMY_CAL_WORD2; // (dummy)
	       	calWord3 = DUMMY_CAL_WORD3; // (dummy)
	       	calWord4 = DUMMY_CAL_WORD4; // (dummy)

		c1 = calWord1>>3;

		c2 = calWord1<<10;
		c2 &= 0x1c00;
		c2 += calWord2>>6;

		c3 = calWord3>>6;

		c4 = calWord4>>7;

		c5 = calWord2 & 0x3f;
		c5 <<= 6;
		c5 |= (calWord3 & 0x3f);

		c6 = calWord4 & 0x7f;

		// read pressure and temperature from gascell sensor
		d1   = DUMMY_D1; // (dummy)
		d2   = DUMMY_D2; // (dummy)
		ut20 = (c5<<3) + 10000;
		dut  = d2 - ut20;

		off  = c2 + (((c4-250)*dut)>>12) + 10000;     //off  = c2 + (c4-250)*dut/4096 + 10000;
		sens = (c1>>1) + (((c3+200)*dut)>>13) + 3000; //sens = c1/2 + (c3+200)*dut/8192 + 3000;
		p    = ((sens*(d1-off))>>12) + 1000;          //p    = sens*(d1-off)/4096 + 1000; // new pressure readout


		// The following term dut2 is the optional 2nd order temperature  
		// calculation for a most accurate temperature reading            
		if (dut<0)
			dut2 = dut - (((dut>>7)*(dut>>7))>>2); //dut2 = dut - (dut/128*dut/128)/4;
		else
			dut2 = dut - (((dut>>7)*(dut>>7))>>3); //dut2 = dut - (dut/128*dut/128)/8;

		t = 200 + ((dut2*(c6+100))>>11); //t = 200+dut2*(c6+100)/2048; // new temperature readout

		// (In this simulation, divisions by powers of 2 have been replaced by appropriate bit right-shifts
		//  so as to avoid costly division operations.
		//  The modification in 'sens' results in a difference of -4 while that in 'p' alone results in a difference
		//  of +1. Overall 'p' difference is thus -3 throughout all calculations.
		//  The modification in 'dut2' results in differences of ~ -1 to -1.2 while that of 't' results in differences
		//  of ~ +0.1 to +1.0.)

		p_accum += p;
		t_accum += t;
	}

	// globally update pressure and temperature results
	if ( (DUMMY_ICA_REFRESH == TRUE) || (DUMMY_GASCELL_CHECK == TRUE) || (DUMMY_RECORD_RESULTS == TRUE) ) {
		enablePressPort = FALSE; // (dummy)
		return;			 // read from file if this function is not called by gascellSwitchSafetyCheck() or by makeGascellSwitchDecision()
	}

	lastAveragePressure = (float)(p_accum) / PRESSURE_READINGS_TO_AVERAGE;
	lastAverageTemp     = (float)(t_accum) / PRESSURE_READINGS_TO_AVERAGE / 10.0; // divide by 10 at the end to get the right units

	lastAveragePressure = (long)read_data(fd_press); // Read actual data from files to emulate normal system behavior
	lastAverageTemp     = (long)read_data(fd_temp);  // (2 single file reads do not impact simulation noticeably)

	enablePressPort = FALSE; // (dummy)
}


// Gets the actual integrated current value sourced from the gascell monitor. Looks both the stored variable and the remainder
// in the gascell monitor peripheral IC. If the ICA is getting low, it tops it up.
// Units: mAhr
float getActualIntegratedI_mAhr() {
	int ICA_register;

	ICA_register = full_gas_mon_ICA - current_ICA;
	partialActualIntegratedI_mAhr += ((float)ICA_register)/(2048.0*gascellSenseResistor)*1000.0; // increment and convert to mAhr

	if (current_ICA < ICA_TOP_UP_LIMIT)
		current_ICA = full_gas_mon_ICA; // fill it right up again since it counts down, not up

	if ( (DUMMY_ICA_REFRESH == TRUE) || (DUMMY_GASCELL_CHECK == TRUE) || (DUMMY_RECORD_RESULTS == TRUE) ) {
		return partialActualIntegratedI_mAhr;	// if this function is called by ICA_refresh_check() or gascellSwitchSafetyCheck(), don't read actual data
							// from the input file, since they are exactly as many as required by the makeGascellSwitchDecision() function
	}

	partialActualIntegratedI_mAhr = read_data(fd_current); // Read actual data from files to emulate normal system behavior
							       // (1 single file read does not impact simulation noticeably)
	//printf("\n[%f]", partialActualIntegratedI_mAhr); // DEBUG

	return partialActualIntegratedI_mAhr; // remember that the ICA counts down
}


// The ICA cannot hold the capacity of a full gascell so the ICA is read and filled up periodically.  The accumulation is updated
// and held in actualIntegratedmAhr. Must call every 1/2 hour or so because it can only hold about 30mAhr with the present set up.
void ICA_refresh_check() {
	DUMMY_ICA_REFRESH = TRUE;  //* (dummy)
	getActualIntegratedI_mAhr();  // performs refresh as part of reading
	DUMMY_ICA_REFRESH = FALSE; //*
}


float getEstimatedVolume_mL_drugNotGas(float actualIntegratedI_mAhr, long profile_time) {
	if (profile_time > 0) {
        	return getEstimatedVolume_mL_gasNotDrug(actualIntegratedI_mAhr);
		// The actualIntegratedI_mAhr is zeroed after pressurisation so this gives the required value
	}
	else
		return 0.0;
}


// Gets the estimated gas volume released based purely on integrated current and the mAhr/mL factor. This is different than the volume of drug released
// because of the amount of gas required to pressure the syringe before any drug is dispensed.
// Units: mL
float getEstimatedVolume_mL_gasNotDrug (float actualIntegratedI_mAhr) {
	return (actualIntegratedI_mAhr/INTEGRATED_I_TO_VOLUME_FACTOR_mAhr_per_mL);
}


// Gets the target drug volume that should have been dispensed based on the profile curve and the current time.  This is the area under the graph from
// time = 0 to the current time.  "time = 0" is taken to mean time at pressurisation complete.  To get convert from a time on the delivery graph to system time, 
// add timeAtPressurisation. time = 0 on the profile graph is when the syringe is pressurised. Will return zero if profile time is negative.
// Units: mL
float getTargetVolume_mL(long profile_time) {
	float area; // area accumulation calculation
	float rise, run, slope, width, side_sum;
	unsigned int i; // profile index
	dataPoint inBetweenVertex; // point profile that current time represents
				   // this is usually between two programmed vertices

	area = 0.0;
	i = 0;

	if (profile_time > 0) {
		// only add next segment if it ends before the present. "Next segment" means a 
		// right hand vertex of activeDeliveryProfile[i+1]
		while (activeDeliveryProfile[i+1].minute < profile_time) {

			//add on next area:   1/2 sum of parallel sides * width.  Units mL/day*minutes
			side_sum = activeDeliveryProfile[i].rate + activeDeliveryProfile[i+1].rate;
			width = activeDeliveryProfile[i+1].minute - activeDeliveryProfile[i].minute;
			area += side_sum/2*width;
			i++;
		}

		// need to add on that part of the curve from last vertex to the current time.
		// First, find the in between vertex at given by the current profile time

		// find the rate at this instant by straight line interpolation
		// i is pointing to the most recent point on the graph in the past.
		// i+1 is the first point in the future
		inBetweenVertex.minute = profile_time;

		// slope of whole of next section
		rise = activeDeliveryProfile[i+1].rate - activeDeliveryProfile[i].rate;
		// width of whole next section
		run = activeDeliveryProfile[i+1].minute - activeDeliveryProfile[i].minute;
		//if (run == 0 ) run = 1; // If user entered vertical slope, make an approximation
		slope = rise/run;

		// width of part of next profile section, not whole section
		width = inBetweenVertex.minute - activeDeliveryProfile[i].minute;  
		inBetweenVertex.rate =  activeDeliveryProfile[i].rate + slope * width;

		//add on next area:   1/2 sum of parallel sides * width.  Units mL/day*minutes
		side_sum = activeDeliveryProfile[i].rate + inBetweenVertex.rate;
		area += side_sum/2*width;

		//Convert from mL/day*minutes to mL
		return (area/24.0/60.0);
	}
	else
		return 0.0;
}


void UpdateTiltMotionCyclesAndPercentOpen() {

	// accumulate count
	accumulatedMotionCycles += 0xFFFF - motion_cycles_counter;  // counter counts down
	//printf("\nAccumulated motion cycles so far: [%ld]", accumulatedMotionCycles); // $DEBUG$

	// reset counter
	motion_cycles_counter = 0xFFFF; // reset timer - can only be a down counter in event counting mode


	// accumulate count
	accumulatedMotionPercentOpen += 0xFFFF - motion_percentopen_counter;  // counter counts down
	//printf("\nAccumulated motion percent-open so far: [%1.2f%%]", accumulatedMotionPercentOpen); // $DEBUG$

	// reset counter
	motion_percentopen_counter = 0xFFFF;  // reset timer - can only be a down counter in event counting mode
}


void resetTiltMotionCyclesAndPercentOpen() {
	// reset tilt motion cycles
	resultMotionCycles = (float)(accumulatedMotionCycles*100L/RECORD_RESULTS_PERIOD*60L); // normalized motion cycles by recording period

	if (resultMotionCycles > 100.0) // just in case it's bigger than expected, limit it so that it will fit when
		resultMotionCycles = 100.0; // converted to a byte for storage

	accumulatedMotionCycles = 0; // clear for beginning of next results period

	// reset tilt motion percent-open
	resultMotionPercentOpen = (float)(accumulatedMotionPercentOpen*100L/RECORD_RESULTS_PERIOD*60L); // normalized motion percent-open by recording period

	if (resultMotionPercentOpen > 100.0)     // just in case it's bigger than expected, limit it so that it will fit when
		resultMotionPercentOpen = 100.0; // converted to a byte for storage

	accumulatedMotionPercentOpen = 0.0; // clear for beginning of next results period
}


void gascellSwitchSafetyCheck() {
	float getPressure, getTemp;

	DUMMY_GASCELL_CHECK = TRUE;  //* (dummy) for kernel correct operation, reading pressure/temperature from file is disabled since not enough entries exist
				                 //* (similar to the DUMMY_ICA_REFRESH flag)

	doPressAndTempConversion(); // for getting pressure and temperature

	getPressure = lastAveragePressure + pressureCalOffset;
	getTemp = lastAverageTemp + tempFromPressSensorCalOffset;

	// takes precedence over all else
	if ( (getPressure > MAX_SAFE_PRESSURE) || (getEstimatedVolume_mL_gasNotDrug(getActualIntegratedI_mAhr()) > MAX_SAFE_GAS_VOLUME)
		|| (getTemp < MIN_SAFE_TEMPERATURE) || (getTemp > MAX_SAFE_TEMPERATURE) ) {
		gascell_on = FALSE; // (dummy)
	}
	DUMMY_GASCELL_CHECK = FALSE; //*
}
